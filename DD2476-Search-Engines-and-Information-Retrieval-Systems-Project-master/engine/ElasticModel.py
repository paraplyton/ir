from pathlib import Path
from elasticsearch import Elasticsearch
import json
import glob
import re

from flask import Flask
app = Flask(__name__)

def ElasticModel(input_query, input_index):
    es = Elasticsearch();
    raw_query = input_query
    wild_query = re.sub(r'(\s)', r'* ', raw_query)
    wild_query += '*'

    try:
        es.indices.refresh(index=input_index)
        res = es.search(index=input_index, body={
            '_source' : 'answers',
            'query' : {
                'wildcard' : {
                    'question' : wild_query
                }
            }
        })
        wild_result = res['hits']['hits']
        if wild_result:
            output = wild_result[0]['_source']['answers'][0]['answer']

        else:
            res = es.search(index=input_index, body={
                '_source': 'answers',
                'query': {
                    'match': {
                        'question' : raw_query
                    }
                }
            })
            raw_result = res['hits']['hits']
            if raw_result:
                output = raw_result[0]['_source']['answers'][0]['answer']
            else:
                output = ''

        return output

    except Exception as e:
        count = 1
        # create, update index on the fly
        # parse URL istället för 2:or
        # make-fil, indexering etc.
        path = str(Path().absolute().parent)
        path += '/crawler/first_crawler/'
        name = input_index
        ending = '.json'
        es.indices.create(index=input_index)

        for name in glob.glob(path+name+ending):
            json_data = open(name).read()
            data = json.loads(json_data)
            if (len(data) > 1):
                for d in data:
                    res = es.index(index=input_index, doc_type='csv', id=count, body=d)
                    count += 1
            else:
                res = es.index(index=input_index, doc_type='csv', id=count, body=data)
                count += 1
    else:
        pass
    finally:
        pass

@app.route("/search/<args>")
def search(args):
    print(args)
    args_ = args.split("2")
    query_string = ""
    for s in args_:
        query_string += s + " "
    query_string = query_string[:-1]
    print(query_string)
    return ElasticModel(query_string, 'life_hacks')

if __name__ == "__main__":
    app.run()
