/*  
 *   This file is part of the computer assignment for the
 *   Information Retrieval course at KTH.
 * 
 *   Johan Boye, 2017
 */  

/**
 *  Copy of searcher class. 4/3-2019. Assignment 2.1 is working, but not 2.2
 */


package ir;

import java.util.*;

/**
 *  Searches an index for results of a query.
 */
public class Searcher {

    /** The index to be searched by this Searcher. */
    Index index;

    /** The k-gram index to be searched by this Searcher */
    KGramIndex kgIndex;
    
    /** Constructor */
    public Searcher( Index index, KGramIndex kgIndex ) {
        this.index = index;
        this.kgIndex = kgIndex;
    }

    /**
     *  Searches the index for postings matching the query.
     *  @return A postings list representing the result of the query.
     */
    public PostingsList search( Query query, QueryType queryType, RankingType rankingType ) { 
        // Get the postings lists for all search terms and store them in a list.

        List<Query.QueryTerm> querytermIDList = query.queryterm;
        List<PostingsList> postingsListList = new ArrayList<PostingsList>();

        for (int i=0; i<query.size(); i++) {
            Query.QueryTerm queryterm = querytermIDList.get(i);
            String queryToken = queryterm.term;
            PostingsList queryPostingsList = index.getPostings( queryToken );
            int corpusLength = index.getLastDocID();
            // If there's only a single search term then returns its postings list.
            // make this prettier when everything works.
            if ( query.size() == 1 && !queryType.equals(QueryType.RANKED_QUERY) ) {
                return queryPostingsList;
            } else {
            postingsListList.add( queryPostingsList );
            }
        }

        if ( queryType.equals( QueryType.INTERSECTION_QUERY ) ) {
            return intersect( postingsListList );
        } else if ( queryType.equals( QueryType.PHRASE_QUERY ) ) {
            return phraseIntersect( postingsListList );
        } else if ( queryType.equals( QueryType.RANKED_QUERY ) ) {
            return cosineScore(postingsListList);
        } else {
            return null;
        }
    }

    private PostingsList cosineScore( List<PostingsList> postingsListArray ) {
        // Change scores in postingsEntry.java. Sort them according to scores,
        // save sorted list of postings entries and then RESET SCORES TO ZERO.

        // The lists "scores" and "length" should have the same size
        // as the number of documents that are scored.
        PostingsList postingsList = postingsListArray.get( 0 );
        if ( postingsList == null ) {
            return null;
        }
    
        int corpusLength = index.getLastDocID() + 1 ;// number of docs in corpus.
        int queryLength = postingsListArray.size(); // this is equal to 1 in task 2.1
        int noOfDocs = postingsList.size(); // only one query term in 2.1
        //List<Float> scores = new ArrayList<Float>( noOfDocs );
        //List<Float> length = new ArrayList<Float>( noOfDocs );
        List<PostingsEntry> sortingList = new ArrayList<PostingsEntry>( noOfDocs );
        
        // Calculate tf_idf weight for the query term.
        
        double queryIdf = Math.log( corpusLength/noOfDocs );
        // for each document
        for ( int i=0; i<noOfDocs; i++ ) {
            PostingsEntry postingsEntry = postingsList.get( i );
            int docID = postingsEntry.docID;
            int docLength = index.docLengths.get( docID );
            //length.set( i, docLength );
            int termFreq = postingsEntry.termIDList.size();
            // shouldn't Math.log() be called here as in the queryTfIdf case?
            double newScore = Math.pow(queryIdf,2)*termFreq/(docLength*queryLength);
            //scores.set( i, score );
            postingsEntry.score = newScore;
            sortingList.add( postingsEntry );
        }
        
        Collections.sort( sortingList );
        PostingsList resultList = new PostingsList();
        for ( int i=0; i<noOfDocs; i++ ) {
            PostingsEntry postingsEntry = sortingList.get( i );
            resultList.add( postingsEntry );
            //postingsEntry.score = 0;
        }

        return resultList;
    }

    // this function works for single word queries. Adjust so it works for multi.
    // The algorithm needs to be changed and perhaps a data structure for docIDs is needed.
    /*private PostingsList cosineScore( List<PostingsList> postingsListArray ) {
        // change scores in postingsEntry.java. sort them according to scores,
        // save sorted list of postings entries and then reset scores to zero.

        // The lists "scores" and "length" should have the same size
        // as the number of documents that are scored.
        PostingsList postingsList = postingsListArray.get( 0 );
        if ( postingsList == null ) {
            return null;
        }
        // RESOLVE THIS!!
        int corpusLength = index.getLastDocID() + 1 ;// number of docs in corpus.
        int queryLength = postingsListArray.size(); // this is equal to 1 in task 2.1
        int noOfDocs = postingsList.size(); // only one query term
        //List<Float> scores = new ArrayList<Float>( noOfDocs );
        //List<Float> length = new ArrayList<Float>( noOfDocs );
        List<PostingsEntry> sortingList = new ArrayList<PostingsEntry>( noOfDocs );
        
        // Calculate tf_idf weight for the query term.
        
        double queryIdf = Math.log( corpusLength/noOfDocs );
        // for each document
        for ( int i=0; i<noOfDocs; i++ ) {
            PostingsEntry postingsEntry = postingsList.get( i );
            int docID = postingsEntry.docID;
            int docLength = index.docLengths.get( docID );
            //length.set( i, docLength );
            int termFreq = postingsEntry.termIDList.size();
            // shouldn't Math.log() be called here as in the queryTfIdf case?
            double newScore = Math.pow(queryIdf,2)*termFreq/(docLength*queryLength);
            //scores.set( i, score );
            postingsEntry.score = newScore;
            sortingList.add( postingsEntry );
        }
        
        Collections.sort( sortingList );
        PostingsList resultList = new PostingsList();
        for ( int i=0; i<noOfDocs; i++ ) {
            PostingsEntry postingsEntry = sortingList.get( i );
            resultList.add( postingsEntry );
            //postingsEntry.score = 0;
        }

        return resultList;
    }
*/
    /** Takes multiple posting lists and returns the intersection of them. */
    private PostingsList intersect( List<PostingsList> postingsListArray ) {
        Collections.sort( postingsListArray ); 
        PostingsList postingsList = postingsListArray.get(0);
        PostingsList otherPostingsList = postingsListArray.get(1);
        // Check if either postings lists is null and if so return null.
        if ( postingsList == null || otherPostingsList == null ) {
            return null;
        }
        
        PostingsList intersectedPostings = new PostingsList();
        int i=0, j=0;
        int currentDocID;
        int otherDocID;
        // Intersection is done when all docIDs in at least one of the lists have been checked.
        while ( i < postingsList.size() && j < otherPostingsList.size() ) {
            PostingsEntry currentEntry = postingsList.get(i);
            currentDocID = postingsList.get(i).docID;
            otherDocID = otherPostingsList.get(j).docID;
            // If the docIDs are equal, add it to the intersection list.
            if ( currentDocID == otherDocID ) {
                intersectedPostings.add( currentEntry );
                i++;
                j++;
            // Increase the index by one corresponding to the list with the currently smallest docID.
            } else if ( currentDocID < otherDocID ) {
                i++;
            } else {
                j++;
            }
        }
        // When there are more than two words in the query
        // the intersection is handled through recursion.
        int postingsListsLeft = postingsListArray.size();
        if ( postingsListsLeft > 2 ) {
            List<PostingsList> newInput = new ArrayList<PostingsList>();
            List<PostingsList> restOfList = postingsListArray.subList(2, postingsListsLeft );
            newInput.add(intersectedPostings);
            newInput.addAll( restOfList );
            return intersect( newInput );
        } else {
            return intersectedPostings;    
        }
    }
    
    /** Returns the postings lists which contains the queried phrase. */
    private PostingsList phraseIntersect( List<PostingsList> postingsListArray ) {
        // The first two words in the list are checked for phrase intersection.
        PostingsList postingsList = postingsListArray.get(0);
        PostingsList otherPostingsList = postingsListArray.get(1);
        // Check if either postings lists is null and if so return null.
        if ( postingsList == null || otherPostingsList == null ) {
            return null;
        }
        // Get the smallest docID for both lists.
        PostingsList intersectedPostings = new PostingsList();
        int i=0, j=0;        
        // Intersection is done when all docIDs in at least one of the lists have been checked.
        while ( i < postingsList.size() && j < otherPostingsList.size() ) {
            PostingsEntry currentEntry = postingsList.get(i);
            PostingsEntry otherEntry = otherPostingsList.get(j);
            int currentDocID = currentEntry.docID;
            int otherDocID = otherEntry.docID;
            // If the docIDs are equal, add it to the intersection list.
            if ( currentDocID == otherDocID ) {
                List<Integer> currentList = currentEntry.termIDList;
                List<Integer> otherList = otherEntry.termIDList;
                List<Integer> comparisonList = new LinkedList<Integer>();
                for ( int k=0; k<currentList.size(); k++ ) {
                    comparisonList.add( currentList.get(k) + 1 );
                }         
                if ( comparisonList.size() < otherList.size() ) {
                    for ( int l=0; l<comparisonList.size(); l++ ) {
                        int termID = comparisonList.get(l);
                        if ( otherList.contains( termID ) ) {
                            intersectedPostings.add( currentDocID, termID );
                        }
                    }
                } else {
                    for ( int l=0; l<otherList.size(); l++ ) {
                        int termID = otherList.get(l);
                        if ( comparisonList.contains( termID ) ) {
                            intersectedPostings.add( currentDocID, termID );
                        }
                    }
                }
                i++;
                j++;
            // Increase the index by one corresponding to the list with the currently smallest docID.
            } else if ( currentDocID < otherDocID ) {
                i++;
            } else {
                j++;
            }
        }
        // When there are more than two words in the query phrase
        // they are handled recursively.
        int inputSize = postingsListArray.size();
        if ( inputSize > 2 ) {
            List<PostingsList> newInput = new ArrayList<PostingsList>();
            List<PostingsList> restOfList = postingsListArray.subList( 2, inputSize );
            newInput.add( intersectedPostings );
            newInput.addAll( restOfList );
            return phraseIntersect( newInput );
        } else {
            return intersectedPostings;    
        }   
    }      
}