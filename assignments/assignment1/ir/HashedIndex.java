/*  
 *   This file is part of the computer assignment for the
 *   Information Retrieval course at KTH.
 * 
 *   Johan Boye, 2017
 */  


package ir;

import java.util.HashMap;
import java.util.Iterator;


/**
 *   Implements an inverted index as a Hashtable from words to PostingsLists.
 */
public class HashedIndex implements Index {

    /** The index as a hashtable. */
    private HashMap<String,PostingsList> index = new HashMap<String,PostingsList>();

    /** Mapping from document identifier to document length. */
    public HashMap<Integer,Integer> docLengths = new HashMap<Integer,Integer>();

    private int lastDocID = 0;

    /**
     *  Inserts this token in the hashtable.
     */
    public void insert( String token, int docID, int offset ) {
    	// Check if the token has a postings list.
    	// If not, make one and map it. Then add the docID.
    	// Finally, map the token with the postings list.
    	PostingsList postingsList = getPostings( token );
        if ( postingsList == null ) {
        	postingsList = new PostingsList();
        	index.put( token, postingsList );
        	postingsList.add( docID, offset );
        } else {
    		postingsList.add( docID, offset );
    	}

        // We want to know how many docs are in the index.
        if ( docID != lastDocID ) {
            lastDocID++;
        }
    }

    /**
     *  Returns the postings for a specific term, or null
     *  if the term is not in the index.
     */
    public PostingsList getPostings( String key ) {
        PostingsList value = index.get( key );
        return value;
    }

    public int getLastDocID() {
        return lastDocID;
    }
    /**
     *  No need for cleanup in a HashedIndex.
     */
    public void cleanup() {
    }
}
