/*  
 *   This file is part of the computer assignment for the
 *   Information Retrieval course at KTH.
 * 
 *   Johan Boye, 2017
 */  

package ir;

import java.util.*;

public class PostingsList implements Comparable<PostingsList> {
    
    /** The postings list */
    private List<PostingsEntry> list = new ArrayList<PostingsEntry>();


    /** Number of postings in this list. */
    public int size() {
    return list.size();
    }

    /** Returns the ith posting. */
    public PostingsEntry get( int i ) {
    return list.get( i );
    }

    /** Adds a posting to the list.*/
    public void add( int docID, int offset ) {
        if ( list.isEmpty() ) {
            PostingsEntry postingsEntry = new PostingsEntry();
            postingsEntry.add( docID, offset );
            list.add( postingsEntry );
        } else {
            PostingsEntry lastEntry = list.get( list.size() - 1 );
            int prevDocID = lastEntry.docID;
            // If the token occurs more than once in the document.
            if ( prevDocID == docID ) {
                lastEntry.termIDList.add( offset );
            } else {
                PostingsEntry postingsEntry = new PostingsEntry();
                postingsEntry.add( docID, offset );
                list.add( postingsEntry );
            }    
        }       
    }

    /** Adds a posting to the list. Dirty method, be careful! */
    public void add( PostingsEntry postingsEntry ) {
        list.add( postingsEntry );       
    }
    
    public PostingsEntry getEntryByID( int docID ) {
        for ( int i=0; i<list.size(); i++ ) {
            PostingsEntry postingsEntry = list.get(i);
            if ( docID == postingsEntry.docID ) {
                return postingsEntry;
            }
        }
        return null;
    }

    public int compareTo(PostingsList postingsList) {
        return this.size() - postingsList.size();
    }
}

