/*  
 *   This file is part of the computer assignment for the
 *   Information Retrieval course at KTH.
 * 
 *   Johan Boye, 2017
 */  

package ir;

import java.util.*;

public class PostingsList implements Comparable<PostingsList> {
    
    /** The postings list */
    private List<PostingsEntry> list = new ArrayList<>();


    /** Number of postings in this list. */
    public int size() {
    return list.size();
    }

    /** Returns the ith posting. */
    public PostingsEntry get( int i ) {
    return list.get( i );
    }

    /** Adds a posting to the list.*/
    public void add( int docID ) {
        PostingsEntry postingsEntry = new PostingsEntry();
        postingsEntry.add( docID );
        list.add( postingsEntry );
    }

    public int compareTo(PostingsList postingsList) {
        return this.size() - postingsList.size();
    }
}

