/*  
 *   This file is part of the computer assignment for the
 *   Information Retrieval course at KTH.
 * 
 *   Johan Boye, 2017
 */  

package ir;

import java.util.*;

/**
 *  Searches an index for results of a query.
 */
public class Searcher {

    /** The index to be searched by this Searcher. */
    Index index;

    /** The k-gram index to be searched by this Searcher */
    KGramIndex kgIndex;
    
    /** Constructor */
    public Searcher( Index index, KGramIndex kgIndex ) {
        this.index = index;
        this.kgIndex = kgIndex;
    }

    /**
     *  Searches the index for postings matching the query.
     *  @return A postings list representing the result of the query.
     */
    public PostingsList search( Query query, QueryType queryType, RankingType rankingType ) { 
        // Get the postings lists for all search terms and store them in a list.
        List<Query.QueryTerm> querytermList = query.queryterm;
        List<PostingsList> postingsListList = new ArrayList<PostingsList>();

        for (int i=0; i<query.size(); i++) {
            Query.QueryTerm queryterm = querytermList.get(i);
            String queryToken = queryterm.term;
            PostingsList queryPostingsList = index.getPostings( queryToken );
            // If there's only a single search term then returns its postings list.
            if ( query.size() == 1 ) {
                return queryPostingsList;
            } else {
            postingsListList.add( queryPostingsList );
            }
        }

        if ( queryType == QueryType.INTERSECTION_QUERY ) {
        return intersectMulti( postingsListList );
        } else if ( queryType == QueryType.PHRASE_QUERY ) {
            return null;
        } else {
            return null;
        }
    }
    /* Takes multiple posting lists and returns the intersection of the lists. */
    // Perhaps the two intersect functions can be combined into one recursive function?
    private PostingsList intersectMulti( List<PostingsList> postingsListArray ) {
        List<PostingsList> orderedArray = postingsListArray;
        Collections.sort(orderedArray);
        PostingsList p1 = orderedArray.get(0);
        PostingsList p2 = orderedArray.get(1);
        PostingsList intersection = intersectPair( p1, p2 );
        
        for ( int i=2; i<orderedArray.size(); i++ ) {

            intersection = intersectPair( intersection, orderedArray.get(i) );
        }

        return intersection;
    }

    /** Takes two posting lists and returns the intersection of the lists. */
    private PostingsList intersectPair( PostingsList p1, PostingsList p2 ) {
        PostingsList postingsList = p1;
        PostingsList otherPostingsList = p2;
        // Check if either postings lists is null and if so return null.
        if ( postingsList == null || otherPostingsList == null ) {
            return null;
        }
        // Get the smallest docID for both lists.
        PostingsList intersectedPostings = new PostingsList();
        int i=0, j=0;
        int currentDocID;
        int otherDocID;
        // Intersection is done when all docIDs in at least one of the lists have been checked.
        while ( i < postingsList.size() && j < otherPostingsList.size() ) {
            currentDocID = postingsList.get(i).docID;
            otherDocID = otherPostingsList.get(j).docID;

            // If the docIDs are equal, add it to the intersection list.
            if ( currentDocID == otherDocID ) {
                intersectedPostings.add( currentDocID );
                i++;
                j++;
            // Increase the index by one corresponding to the list with the currently smallest docID.
            } else if ( currentDocID < otherDocID ) {
                i++;
            } else {
                j++;
            }
        }

        return intersectedPostings;
    }
        
}