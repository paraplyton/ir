filetext = fileread('JonasEriksson.txt');
expr = '(?<=.f )\d';
matches = regexp(filetext,expr,'match');
numvector = []; 
[temp, len] = size(matches);
for i = 1:len
    numvector(i) = str2num(matches{i});
end

num_of_good_docs = 100;
ranked_vector = numvector(len-50+1:end);
unranked_vector = numvector(1:len-50);
ranked_precision = [];
ranked_recall = [];
unranked_precision = [];
unranked_recall = [];
counter = 0;
for j = 1:length(ranked_vector)
    if ranked_vector(j) ~= 0;
        counter = counter + 1;
    end
    ranked_precision(j) = counter/j;
    ranked_recall(j) = counter/100;
end
counter = 0;
for k = 1:length(unranked_vector)
    if unranked_vector(k) ~= 0;
        counter = counter + 1;
    end
    unranked_precision(k) = counter/k;
    unranked_recall(k) = counter/100;
end

precisions = ranked_precision(10:10:end)
recalls = ranked_recall(10:10:end)

%%
close all
figure(1)
hold on
plot(ranked_precision);
plot(ranked_recall);
title('Ranked retrieval');
legend('Precision', 'Recall', 'Location', 'NorthWest');

figure(2)
hold on
plot(unranked_precision);
plot(unranked_recall);
title('Unranked retrieval');
xlabel('Numberof inspected docs');
legend('Precision', 'Recall', 'Location', 'NorthEast');

figure(3)
hold on
plot(ranked_recall, ranked_precision);
title('Precision-recall curve');
max_val = max(max(ranked_recall),max(ranked_precision));
axis([0 max_val 0 max_val]);
xlabel('Recall (sensitivity)');
ylabel('Precision (PPV)');
%%
