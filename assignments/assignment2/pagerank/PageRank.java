import java.util.*;
import java.io.*;

public class PageRank {

    /**  
     *   Maximal number of documents. We're assuming here that we
     *   don't have more docs than we can keep in main memory;
     */
    final static int MAX_NUMBER_OF_DOCS = 1000;

    /**
     *   Mapping from document names to document numbers.
     */
    Hashtable<String,Integer> docNumber = new Hashtable<String,Integer>();

    /**
     *   Mapping from document numbers to document names
     */
    String[] docName = new String[MAX_NUMBER_OF_DOCS];

    /**  
     *   The transition matrix. p[i][j] = the probability that the
     *   random surfer clicks from page i to page j.
     */
    double[][] p = new double[MAX_NUMBER_OF_DOCS][MAX_NUMBER_OF_DOCS];

    /**
     *   The number of outlinks from each node.
     */
    int[] out = new int[MAX_NUMBER_OF_DOCS];

    /**
     *   The probability that the surfer will be bored, stop
     *   following links, and take a random jump somewhere.
     */
    final static double BORED = 0.15;

    /**
     *   In the initializaton phase, we use a negative number to represent 
     *   that there is a direct link from a document to another.
     */
    final static double LINK = -1.0;
    
    /**
     *   Convergence criterion: Transition probabilities do not 
     *   change more that EPSILON from one iteration to another.
     */
    final static double EPSILON = 0.0001;

    
    /* --------------------------------------------- */


    public PageRank( String filename ) {
	int noOfDocs = readDocs( filename );
	initiateProbabilityMatrix( noOfDocs );
	iterate( noOfDocs, 100 );
    }


    /* --------------------------------------------- */


    /**
     *   Reads the documents and fills the data structures. When this method 
     *   finishes executing, <code>p[i][j] = LINK</code> if there is a direct
     *   link from i to j, and <code>p[i][j] = 0</code> otherwise.
     *   <p>
     *
     *   @return the number of documents read.
     */
    int readDocs( String filename ) {
	int fileIndex = 0;
	try {
	    System.err.print( "Reading file... " );
	    BufferedReader in = new BufferedReader( new FileReader( filename ));
	    String line;
	    while ((line = in.readLine()) != null && fileIndex<MAX_NUMBER_OF_DOCS ) {
    		int index = line.indexOf( ";" );
    		String title = line.substring( 0, index );
    		Integer fromdoc = docNumber.get( title );
    		//  Have we seen this document before?
    		if ( fromdoc == null ) {	
    		    // This is a previously unseen doc, so add it to the table.
    		    fromdoc = fileIndex++;
    		    docNumber.put( title, fromdoc );
    		    docName[fromdoc] = title;
    		}
    		// Check all outlinks.
    		StringTokenizer tok = new StringTokenizer( line.substring(index+1), "," );
    		while ( tok.hasMoreTokens() && fileIndex<MAX_NUMBER_OF_DOCS ) {
    		    String otherTitle = tok.nextToken();
    		    Integer otherDoc = docNumber.get( otherTitle );
    		    if ( otherDoc == null ) {
    			// This is a previousy unseen doc, so add it to the table.
        			otherDoc = fileIndex++;
        			docNumber.put( otherTitle, otherDoc );
        			docName[otherDoc] = otherTitle;
    		    }
    		    // Set the probability to LINK for now, to indicate that there is
    		    // a link from d to otherDoc.
    		    if ( p[fromdoc][otherDoc] >= 0 ) {
        			p[fromdoc][otherDoc] = LINK;
        			out[fromdoc]++;
    		    }
    		}
	    }
	    if ( fileIndex >= MAX_NUMBER_OF_DOCS ) {
		  System.err.print( "stopped reading since documents table is full. " );
	    
	    } else {
		  System.err.print( "done. " );
	    }
	}
	catch ( FileNotFoundException e ) {
	    System.err.println( "File " + filename + " not found!" );
	
	} catch ( IOException e ) {
	    System.err.println( "Error reading file " + filename );
	}

	System.err.println( "Read " + fileIndex + " number of documents" );
	return fileIndex;
    }

    /* --------------------------------------------- */


    /*
     *   Initiates the probability matrix. 
     */
    void initiateProbabilityMatrix( int numberOfDocs ) {
        // Non-fancy approach
        double teleportElement = 1.0/numberOfDocs;
        double surfElement;
        int counter;
        // Get the number of non-zero entries on each row.
        for ( int i=0; i<numberOfDocs; i++ ) {
            counter = 0;
            for ( int j=0; j<numberOfDocs; j++ ) {
                if ( p[i][j] == LINK ) {
                    counter++;
                }
            }
            // Fill the rows with correct elements.
            if ( counter == 0 ) { 
                Arrays.fill( p[i], teleportElement );
            } else {
                surfElement = 1.0/counter;   
                for ( int k=0; k<numberOfDocs; k++ ) {
                    if( p[i][k] == LINK ) {
                        p[i][k] = (1.0 - BORED)*surfElement;
                    }
                    p[i][k] += BORED*teleportElement;
                }             
            }
        }
    }


    /* --------------------------------------------- */


    /*
     *   Chooses a probability vector a, and repeatedly computes
     *   aP, aP^2, aP^3... until aP^i = aP^(i+1).
     */
    void iterate( int numberOfDocs, int maxIterations ) {

    // YOUR CODE HERE
        double[] preState = new double[numberOfDocs];
        double[] postState = new double[numberOfDocs];
        double[] diff = new double[numberOfDocs];
        int iter = 0;
        Arrays.fill(postState, 1.0/numberOfDocs);
        double norm = 1;
        while ( norm>EPSILON && iter<maxIterations ) {
            // Update vector
            for ( int a=0; a<numberOfDocs; a++ ) {
                preState[a] = postState[a];
            }
            // Do matrix multiplication
            double sum = 0.0;
            for ( int j=0; j<numberOfDocs; j++ ) {
                double elem = 0.0;
                for ( int i=0; i<numberOfDocs; i++ ) {
                    elem += preState[i]*p[i][j];
                }
                postState[j] = elem;
                sum += elem;
            }
            // Normalize new vector (L1)
            for ( int n=0; n<numberOfDocs; n++ ) {
                postState[n] /= sum;
            }
            // Get norm of difference (L1)
            norm = 0.0;
            for ( int l=0; l<numberOfDocs; l++ ) {
                diff[l] = postState[l] - preState[l];
                norm += Math.abs(diff[l]);
            }

            iter++;
        } 

        int numOfPages = 30;
        System.out.println("\nNumber of iterations is "+iter+ ".\nThe "+numOfPages+" highest ranked pages are:\n");
        // The postState vector is done. Now extract the highest indices. Map them to docID, and then to fileNames.
        double[] highestRanks = getHighestRanks(postState, numOfPages);

        List<Integer> indexList = new ArrayList<Integer>();
        for ( int i=0; i<numOfPages; i++ ) {
            double pageRank = highestRanks[i];  
            double scale = Math.pow(10, 7);          
            double roundedPageRank = Math.round(pageRank*scale)/scale;
            for ( int j=0; j<numberOfDocs; j++ ) {
                if ( Math.abs(postState[j]-pageRank) < 0.00001 && !indexList.contains(j) ) {
                    indexList.add(j);
                    System.out.println(i+1+": "+ docName[j]+", "+ roundedPageRank);  
                    break;
                }
                
            }

        }

    }

    /* --------------------------------------------- */

    double[] getHighestRanks( double[] rankArray, int resultLength) {
        double[] storedRanks = new double[resultLength];
        for ( int i=0; i<resultLength; i++ ) {
            storedRanks[i] = rankArray[i];
        }
        Arrays.sort(storedRanks);
        double minValue = storedRanks[0];
        double element;

        for ( int j=resultLength; j<rankArray.length; j++ ) {
            element = rankArray[j];
            if ( element>minValue ) {
                storedRanks[0] = element;
                Arrays.sort(storedRanks);
                minValue = storedRanks[0];
            }
        }
        double t;
        for ( int i=0; i< resultLength/2; i++) {
            t = storedRanks[i];
            storedRanks[i] = storedRanks[resultLength - i - 1];
            storedRanks[resultLength - i - 1] = t;
        }

        return storedRanks;
    }
    
    /* --------------------------------------------- */
    
    double[] getLowestRanks( double[] rankArray, int resultLength) {
        double[] storedRanks = new double[resultLength];
        for ( int i=0; i<resultLength; i++ ) {
            storedRanks[i] = rankArray[i];
        }
        Arrays.sort(storedRanks);
        double maxValue = storedRanks[resultLength-1];
        double element;

        for ( int j=resultLength; j<rankArray.length; j++ ) {
            element = rankArray[j];
            if ( element<maxValue ) {
                storedRanks[resultLength-1] = element;
                Arrays.sort(storedRanks);
                maxValue = storedRanks[resultLength-1];
            }
        }

        return storedRanks;
    }

    /* --------------------------------------------- */


    public static void main( String[] args ) {
	if ( args.length != 1 ) {
	    System.err.println( "Please give the name of the link file" );
	}
	else {
	    new PageRank( args[0] );
	}
    }
}